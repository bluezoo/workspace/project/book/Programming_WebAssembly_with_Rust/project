##Summary Description

(One-line summary description of the defect encountered.)


##Full Description


###Steps to Reproduce

(How one can reproduce the issue in the running application in production.)
1. Step 1.

2. Step 2.

   ...


###What is the current Defect Behavior?

(What actually happens.)


###What is the Expected Correct Behavior?

(What you should see instead.)


###Relevant Logs and/or Screenshots

(Paste or provide links to any relevant logs - please use code blocks (```) to
format console output, logs, and code as it's very hard to read otherwise.)


##Possible Fixes

(If you can, link to the line of code that might be responsible for the
problem.)


##Unit-test Case or Data to Reproduce Problem (Optional).

(If possible, please create a unit-test case and/or data in a remote repository
that exhibits the problematic behaviour, and link to it here in the bug report)


/label ~IT:Defect ~IS:Investigating
/cc @project-manager
/cc @project-develop
/cc @project-test
/cc @issue-author
/assign @project-test
