## Summary Feature Description

(One-line summary feature description of desired feature.)


## Benefits

(For whom (reference customer(s) and organization(s) and why.)


## Requirements and New Use Cases

(Provide high-level requirements and new use cases or a link to a document
or discussion thread describing them.)

## Links/References


/label ~IT:Enhancement ~IS:Investigating
/cc @project-manager
/cc @project-develop
/cc @project-test
/cc @issue-author
/assign @project-sys
